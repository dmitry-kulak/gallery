import { configureStore } from "@reduxjs/toolkit";
import createSagaMiddleware from "redux-saga";

import { photosSlice } from "./slices/photosSlice";
import { photosWatcher } from "./sagas/photosSaga";

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
  reducer: photosSlice.reducer,
  middleware: [sagaMiddleware],
});

sagaMiddleware.run(photosWatcher);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
