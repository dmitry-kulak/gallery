import { createSlice, createAction } from "@reduxjs/toolkit";

import type { Photo } from "../../types/photosTypes";
import type { PayloadAction } from "@reduxjs/toolkit";

export type UsersState = {
  photos: Photo[];
  error: Error | null;
  isPhotosLoading: boolean;
};

const initialState: UsersState = {
  photos: [],
  error: null,
  isPhotosLoading: true,
};

export const photosSlice = createSlice({
  name: "photos",
  initialState,
  reducers: {
    loadPhotosSuccess(state, action: PayloadAction<Photo[]>) {
      state.photos = action.payload;
      state.isPhotosLoading = false;
    },
    loadPhotosFail(state, action: PayloadAction<Error>) {
      state.error = action.payload;
      state.isPhotosLoading = false;
    },
  },
});

export const { loadPhotosSuccess, loadPhotosFail } = photosSlice.actions;

export const loadUsersAction = createAction("photos/loadPhotos");
