import { call, put, takeEvery } from "redux-saga/effects";

import type { AxiosResponse } from "axios";
import type { Photo } from "../../types/photosTypes";
import type { SagaIterator } from "redux-saga";
import { getPhotos } from "../../api/photos.api";
import { loadPhotosFail, loadPhotosSuccess, loadUsersAction } from "../slices/photosSlice";

function* fetchPhotos() {
  try {
    const response: AxiosResponse<Photo[]>[] = yield call(getPhotos);

    const photos = response.flatMap((res) => res.data);
    yield put(loadPhotosSuccess(photos));
  } catch (err) {
    yield put(loadPhotosFail(err as Error));
  }
}

export function* photosWatcher(): SagaIterator {
  yield takeEvery(loadUsersAction.type, fetchPhotos);
}
