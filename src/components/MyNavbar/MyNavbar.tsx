import { useRef, useState, Suspense } from "react";
import { Link, useLocation } from "react-router-dom";

import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Offcanvas from "react-bootstrap/Offcanvas";

import avatar1 from "./avatar1.jpg";
import avatar2 from "./avatar2.png";
import { Photo } from "../Photo/Photo";

export const MyNavbar = () => {
  const [show, setShow] = useState(false);
  const { pathname } = useLocation();

  const toggleShow = () => {
    setShow(!show);
  };
  const avatar = useRef(Math.random() >= 0.5 ? avatar1 : avatar2);

  return (
    <>
      <Navbar bg="light" expand="xs">
        <Container>
          <Navbar.Brand as={Link} to="photos">
            The Gallery
          </Navbar.Brand>
          <Navbar.Toggle onClick={toggleShow} />
        </Container>
      </Navbar>

      <Offcanvas show={show} onHide={toggleShow}>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>The Gallery</Offcanvas.Title>
        </Offcanvas.Header>

        <hr />

        <Offcanvas.Body>
          <Nav variant="pills" className="flex-column">
            <Nav.Item>
              <Nav.Link as={Link} to="photos" active={pathname === "/photos"}>
                Галерея
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link as={Link} to="about" active={pathname === "/about"}>
                Обо мне
              </Nav.Link>
            </Nav.Item>
          </Nav>

          <Container className="d-flex flex-column align-items-center mt-5">
            <Suspense>
              <Photo
                imgSrc={avatar.current}
                title="Аватар"
                className="rounded-circle"
                style={{ width: "150px" }}
              />
            </Suspense>
            <h4 className="mt-3">Дмитрий Кулак</h4>
            <h5>dmitry.kulak11@gmail.com</h5>
          </Container>
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
};
