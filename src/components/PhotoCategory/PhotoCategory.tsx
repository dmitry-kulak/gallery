import { Suspense } from "react";
import { Link } from "react-router-dom";
import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";

import "./PhotoCategory.css";
import type { Album } from "../../types/photosTypes";
import { Photo } from "../Photo/Photo";

type PhotoCategoryProps = {
  photos: Album;
};

export const PhotoCategory = ({ photos }: PhotoCategoryProps) => {
  return (
    <Container>
      <h2>Альбом {photos.albumId}</h2>
      <Container className="d-flex justify-content-center flex-wrap">
        {photos.photos.map((photo) => (
          <div
            className="photo-container d-inline-flex justify-content-center align-items-center m-2 position-relative"
            key={photo.id}
          >
            <Suspense fallback={<Spinner animation="border" />}>
              <Photo imgSrc={photo.thumbnailUrl} title={photo.title} />
              <Link
                className="photo-link position-absolute link-primary bg-white rounded p-1"
                to={`photoId=${photo.id}`}
              >
                Подробнее
              </Link>
            </Suspense>
          </div>
        ))}
      </Container>
    </Container>
  );
};
