import { useImage } from "react-image";

type PhotoProps = {
  imgSrc: string;
  title: string;
  height?: string;
  width?: string;
  className?: string;
  style?: object;
};

export const Photo = ({ imgSrc, title, height, width, className, style }: PhotoProps) => {
  const { src } = useImage({
    srcList: imgSrc,
  });

  return (
    <img src={src} alt={title} height={height} width={width} className={className} style={style} />
  );
};
