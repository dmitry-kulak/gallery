import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";

import { About } from "./pages/About";
import { Gallery } from "./pages/Photos/Gallery";
import { MyNavbar } from "./components/MyNavbar/MyNavbar";
import { PhotoPage } from "./pages/Photos/PhotoPage";
import { Photos } from "./pages/Photos/Photos";

export const App = () => {
  return (
    <BrowserRouter>
      <MyNavbar />

      <Routes>
        <Route index element={<Navigate to="/photos" />} />
        <Route path="photos" element={<Photos />}>
          <Route index element={<Gallery />} />
          <Route path="photoId=:photoId" element={<PhotoPage />} />
        </Route>
        <Route path="about" element={<About />} />
      </Routes>
    </BrowserRouter>
  );
};
