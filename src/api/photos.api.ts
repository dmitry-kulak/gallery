import axios from "axios";
import { instance } from "./api";

import type { Photo } from "../types/photosTypes";

export const getPhotos = () => {
  // gets 6 photos from each of 4 albums
  const endpoints = Array.from(Array(4)).map((_, index) => `photos?albumId=${index + 1}&_limit=6`);

  return axios.all(endpoints.map((endpoint) => instance.get<Photo[]>(endpoint)));
};
