import { Suspense, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Accordion from "react-bootstrap/Accordion";

import { Photo } from "../components/Photo/Photo";

export const About = () => {
  useEffect(() => {
    document.title = "Обо мне";
  }, []);

  return (
    <Container className="mt-3 lead">
      <h1 className="display-2 mb-4 user-select-none">Обо мне (и не только)</h1>
      <p>
        Привет! На этой странице мне нужно рассказать что-нибудь о себе. Основное, относящееся к
        работе, я написал в своем резюме. Здесь будут менее значимые факты, а также что-нибудь про
        это тестовое задание.
      </p>

      <ol>
        <li>
          Когда я делал дополнительную искусственную задержку, я хотел использовать Suspense,
          который с релизом React 18 вышел из экспериментальной стадии и, как мне кажется, подходит
          для этого случая. Но я не разобрался, как использовать это с Redux, и сделал по-старинке.
        </li>
        <li>
          На самом деле Redux был самой простой частью. Больше всего времени я потратил на
          "освоение" бутстрапа.
        </li>
        <li>Эта страница нужна, чтобы проверить, что я знаю React Router.</li>
        <li>Да, я знаю, что style проп — плохая идея.</li>
        <li>Скорее всего, какой-то из плагинов линтера я подключил некорректно.</li>
        <li className="user-select-none">Обожаю, когда невозможно выделять текст на странице.</li>
        <li>
          Недавно я открыл для себя Vite и решил использовать его, когда возможно. Даже такой
          небольшой проект, как этот, вебпаком (с CRA) будет собираться целую минуту против ~10
          секунд у Vite.
        </li>
        <li>Играю на гитаре и пытаюсь освоить саксофон.</li>
        <li>Некоторые люди говорят, что я похож на Валерия Сюткина.</li>
        <li>
          <Accordion flush className="w-50">
            <Accordion.Item eventKey="0">
              <Accordion.Header>Кот</Accordion.Header>
              <Accordion.Body>
                <Suspense>
                  <Photo
                    imgSrc="https://sun9-77.userapi.com/impg/RVuW1XKru2sgCQMKGaOLoPwj_X5Dw6LxaBaUqw/wgjVHYq5z1o.jpg?size=853x1080&quality=95&sign=518774aefed68d48d21f80a4274c6212&type=album"
                    title="dog"
                    width="300px"
                  />
                </Suspense>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </li>
      </ol>
    </Container>
  );
};
