import { useEffect } from "react";
import { Outlet } from "react-router-dom";
import Container from "react-bootstrap/Container";

import "./Photos.css";
import { useAppDispatch, useAppSelector } from "../../store/hooks";
import { loadUsersAction } from "../../store/slices/photosSlice";
import Spinner from "react-bootstrap/Spinner";

export const Photos = () => {
  const isPhotosLoading = useAppSelector((state) => state.isPhotosLoading);
  const dispatch = useAppDispatch();

  useEffect(() => {
    setTimeout(() => dispatch(loadUsersAction()), 500);
  }, []);

  useEffect(() => {
    document.title = isPhotosLoading ? "Загрузка..." : "Галерея";
  }, [isPhotosLoading]);

  return (
    <Container className="d-flex justify-content-center mt-3">
      {isPhotosLoading ? <Spinner animation="border" className="spinner" /> : <Outlet />}
    </Container>
  );
};
