import { useEffect } from "react";
import Container from "react-bootstrap/Container";

import type { Album } from "../../types/photosTypes";
import { useAppSelector } from "../../store/hooks";
import { PhotoCategory } from "../../components/PhotoCategory/PhotoCategory";

export const Gallery = () => {
  const { photos, isPhotosLoading } = useAppSelector((state) => state);

  const albums = photos.reduce<Album[]>((albums, photo) => {
    const { albumId } = photo;

    const albumIndex = albums.findIndex((album) => album.albumId === albumId);

    if (albumIndex === -1) {
      return albums.concat({ albumId, photos: [photo] });
    }

    albums[albumIndex].photos = albums[albumIndex].photos.concat(photo);
    return albums;
  }, []);

  useEffect(() => {
    document.title = isPhotosLoading ? "Загрузка..." : "Галерея";
  }, [isPhotosLoading]);

  return (
    <Container>
      {albums.map((album) => (
        <PhotoCategory photos={album} key={album.albumId} />
      ))}
    </Container>
  );
};
