import { Suspense, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Spinner from "react-bootstrap/Spinner";

import { Photo } from "../../components/Photo/Photo";
import { useAppSelector } from "../../store/hooks";

type PhotoPageParams = {
  photoId: string;
};

export const PhotoPage = () => {
  const [isLoading, setIsLoading] = useState(true);
  const { photoId } = useParams() as PhotoPageParams;
  const { photos } = useAppSelector((state) => state);

  const navigate = useNavigate();
  const goToGallery = () => {
    navigate("../");
  };

  const photo = photos.find((photo) => photo.id === +photoId);

  useEffect(() => {
    setTimeout(() => setIsLoading(false), 500);
  }, []);

  useEffect(() => {
    document.title = isLoading ? "Загрузка..." : `Фото ${photo!.id}`;
  }, [isLoading]);

  if (photo === undefined) {
    return (
      <Container>
        <h1>Такой фотографии нет!</h1>
        <Button variant="primary" onClick={goToGallery}>
          Назад
        </Button>
      </Container>
    );
  }

  return (
    <Container style={{ maxWidth: "600px" }}>
      <Card>
        {isLoading ? (
          <Spinner animation="border" className="spinner" />
        ) : (
          <Suspense fallback={<Spinner animation="border" className="spinner" />}>
            <Photo
              imgSrc={photo.url}
              title={photo.title}
              width="100%"
              style={{ aspectRatio: "1/1" }}
            />
          </Suspense>
        )}

        <Card.Body>
          <Card.Title>{photo.title}</Card.Title>
          <Card.Text>
            Фото с ID {photo.id} из альбома {photo.albumId}, расположенное по адресу{" "}
            <a href={photo.url}>{photo.url}</a>
          </Card.Text>
          <Button variant="primary" onClick={goToGallery}>
            Назад
          </Button>
        </Card.Body>
      </Card>
    </Container>
  );
};
