# gallery

Сайт-галерея, где присутствует 3 страницы (роута):
*  «Галерея» (главная страница), где располагаются картинки;
*  «Обо мне»;
*  Подробности о картинке (краткая информация о картинке – заголовок, id, ссылка)

Используется api https://jsonplaceholder.typicode.com

Деплой на [Netlify](https://dk-gallery.netlify.app/)

## Установка

```sh
$ yarn
```

## Запуск

Для запуска dev сервера

```sh
$ yarn dev
```

Для запуска продакшн билда

```sh
$ yarn build
$ yarn preview
```

## Стэк

* React 18
* Typescript
* Vite
* React-bootstrap
* axios
* Redux-toolkit
* Redux-saga
* React-image